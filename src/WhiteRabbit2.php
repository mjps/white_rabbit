<?php

class WhiteRabbit2
{

    private $coins = [1,2,5,10,20,50,100];
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     * 
     */
    public function findCashPayment($amount){
        $currentAmount = $amount;
        $len = count($this->coins)-1;

        if($amount >= 0){
            for($i = $len; $i >= 0; $i--) { # Go through coins in descending order 100-50-20...

                if($currentAmount >= 0) {
                    $quotient = intdiv($currentAmount,$this->coins[$i]); # quotient of dividen and divisor
        
                    if($quotient >= 1) { 
                        $result[$this->coins[$i]] = $quotient; # populate new array with quotient
                        $currentAmount -= $quotient*$this->coins[$i]; # decrease currentAmount of the quotient*coin (2*2=4) currentAmount-4

                    }
                    else $result[$this->coins[$i]] = 0; # currentAmount is less than divisor, set coin to zero
                }
            }
            return $result;
        }
        else {
            throw new InvalidArgumentException("amount cannot be less than 0. ($amount)");
        }
    }
}
