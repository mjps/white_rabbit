<?php
class WhiteRabbit
{

    private $arrAlphabet;
    private $alphabetOccurrences;


    public function findMedianLetterInFile($filePath) {
        $this->arrAlphabet = range('a','z');
        $this->alphabetOccurrences = array_fill_keys($this->arrAlphabet, 0);
        return array('letter'=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),'count'=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     * 
     * This does not work for specific unicodes, 
     * handling of specific (UTF-8-32) should be considered?
     * 
     * @return populated array with count of alphabetic occurences ('a' => 9240).
     */
    private function parseFile ($filePath) {
        $handle = fopen($filePath, 'r');
        $fr = strtolower(fread($handle, filesize($filePath))); # only read byte size - 1 of $filePath

        $result = preg_replace('/[^a-z]/', '', $fr); # remove any non alphabetic characters and replace with empty string ''
       
        foreach (count_chars($result, 1) as $key => $val) { # count the individual chars (a-z)
            if (array_key_exists(chr($key), $this->alphabetOccurrences)) { # check for key in array (ie. if $key == array[$key])
                $this->alphabetOccurrences[chr($key)] = $val; # set $val of array[$key] to the letter occurence (ie. key: 'm' => val: 9240) 
            }
        }
        fclose($handle);
        
        return $this->alphabetOccurrences;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences) {
        $count = count($parsedFile)-1;
        
        asort($parsedFile); # Sort array to find median. We could also sort the array at the insertion section at line 38.
        $keys = array_keys($parsedFile); # array with keys as 0-25 and values as the alphabet letter (ie.[0] => z,[1] => q)
        $mid = floor($count/2); # middle index pointer of the array

        $median = $keys[$mid]; # letter
        $occurrences = $parsedFile[$keys[$mid]]; #occurences of letter
        
        return $median;


    }

}