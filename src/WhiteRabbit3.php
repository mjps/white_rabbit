<?php

class WhiteRabbit3
{
    /**
     * TODO Goto Test/WhiteRabbit3Test and write some tests that this method will fail
     * You are not allowed to change this method ;)
     * 
     * OBS: The method always returns a round integer round($amount); 
     * 
     * 
     */
    public function multiplyBy($amount, $multiplier){
        echo $amount;
        $estimatedResult = $amount * $multiplier;
        echo $estimatedResult;
        $guess = abs($amount-7);
        while(abs($estimatedResult - $amount) > 0.49 && $guess != 0){
            if($guess > 0.49)
                $guess = $guess / 2;
            if($amount > $estimatedResult){
                $amount = $amount - $guess;
            }
            else{
                $amount = $amount + $guess;
            }
        }
        return round($amount);
    }
}